-- SUMMARY -- 

The Menu Token Config Ignore add a filter to configuration import ot skip menu
token options.

-- REQUIREMENTS --

menu_token
config_filter

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70501 for further information.




